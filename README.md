# rp2040-t1s

[![GPLv3 licensed](https://img.shields.io/badge/license-GPLv3-blue.svg)](https://spdx.org/licenses/GPL-3.0-or-later.html)

Raspberry RP2040 with Single pair Ethernet T1S.

## Breadboard test

* [Raspberry Pi Pico](https://www.raspberrypi.com/products/raspberry-pi-pico/)
* [Two-Wire ETH Click](https://www.mikroe.com/two-wire-eth-click)

## Datasheets

* [RP2040](https://datasheets.raspberrypi.com/rp2040/rp2040-datasheet.pdf)
* [LAN8651](https://www.microchip.com/en-us/product/lan8651)
* [W25Q16JV](https://www.winbond.com/resource-files/w25q16jv%20spi%20revg%2003222018%20plus.pdf)

## Other info

* [HW design with RP2040](https://datasheets.raspberrypi.com/rp2040/hardware-design-with-rp2040.pdf)
* [RP2040 Minimal board](https://datasheets.raspberrypi.com/rp2040/Minimal-KiCAD.zip)

## USB-T1S

* [EVB-LAN8670-USB](https://www.microchip.com/en-us/development-tool/EV08L38A)
* [EVB-LAN8670-USB Driver](https://ww1.microchip.com/downloads/aemDocuments/documents/AIS/ProductDocuments/CodeExamples/EVB-LAN8670-USB_Linux_Driver_1v0.zip)

### Setup on Ubuntu 22.04 (Kernel 5.15)

#### goto the driver sources and build the files

```bash
cd lan867x-linux-driver-1v0/linux-v5.15.84-support
make
ls -1 *.ko

microchip_t1s.ko
smsc95xx_t1s.ko
```
#### insert the modules

```bash
insmod microchip_t1s.ko
modprobe usbnet #standard kernel module
insmod smsc95xx_t1s.ko
```

#### check if the drivers are loaded

```bash
sudo dmesg

LAN867X Rev.B1 usb-001:002:00: PLCA mode enabled. Node Id: 0, Node Count: 8, Max BC: 0, Burst Timer: 128, TO Timer: 32
LAN867X Rev.B1 usb-001:002:00: attached PHY driver (mii_bus:phy_addr=usb-001:002:00, irq=POLL)
smsc95xx_t1s 1-2:1.0 eth0: register 'smsc95xx_t1s' at usb-0000:00:02.0-2, smsc95xx USB 2.0 Ethernet, 00:1e:c0:d1:bb:5c
smsc95xx_t1s 1-2:1.0 enx001ec0d1bb5c: renamed from eth0
```

```bash
ip addr

3: enx001ec0d1bb5c: <BROADCAST,MULTICAST> mtu 1500 qdisc noop state DOWN group default qlen 1000
    link/ether 00:1e:c0:d1:bb:5c brd ff:ff:ff:ff:ff:ff
```

#### configure the new network interface

add to your netplan file

```yaml
    enx001ec0d1bb5c:
      dhcp4: false
      dhcp6: false
      addresses:
        - 192.168.100.1/24
```

apply the updated netplan

```bash
netplan apply
```

you should then have a configure network interface

```bash
ip addr

3: enx001ec0d1bb5c: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc fq_codel state UP group default qlen 1000
    link/ether 00:1e:c0:d1:bb:5c brd ff:ff:ff:ff:ff:ff
    inet 192.168.100.1/24 brd 192.168.100.255 scope global enx001ec0d1bb5c
       valid_lft forever preferred_lft forever
    inet6 fe80::21e:c0ff:fed1:bb5c/64 scope link
       valid_lft forever preferred_lft forever
```

## License

GNU General Public License, Version 3.0 [LICENSE](LICENSE) or
[https://spdx.org/licenses/GPL-3.0-or-later.html](https://spdx.org/licenses/GPL-3.0-or-later.html)
